
(function(){
  'use strict';
  var module = angular.module('app', ['onsen']);
 

       


 
})();


 

// document.addEventListener("deviceready",onDeviceReady,false);

 /*function onDeviceReady() {
        pictureSource=navigator.camera.PictureSourceType;
        destinationType=navigator.camera.DestinationType;
    }
    */

     function onPhotoDataSuccess(imageData) {
      // Uncomment to view the base64-encoded image data
      // console.log(imageData);

      // Get image handle
      //
      var smallImage = document.getElementById('smallImage');

      // Unhide image elements
      //
      smallImage.style.display = 'block';

      // Show the captured photo
      // The inline CSS rules are used to resize the image
      //
      smallImage.src = "data:image/jpeg;base64," + imageData;
    }

    // Called when a photo is successfully retrieved
    //
    function onPhotoURISuccess(imageURI) {
      // Uncomment to view the image file URI
      // console.log(imageURI);

      // Get image handle
      //
      var largeImage = document.getElementById('largeImage');

      // Unhide image elements
      //
      largeImage.style.display = 'block';

      // Show the captured photo
      // The inline CSS rules are used to resize the image
      //
      largeImage.src = imageURI;
    }

    // A button will call this function
    //
    function capturePhoto() {
      // Take picture using device camera and retrieve image as base64-encoded string
      navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 50,
        destinationType: destinationType.DATA_URL });
    }

    // A button will call this function
    //
    function capturePhotoEdit() {
      // Take picture using device camera, allow edit, and retrieve image as base64-encoded string
      navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 20, allowEdit: true,
        destinationType: destinationType.DATA_URL });
    }

    // A button will call this function
    //
    function getPhoto(source) {
      // Retrieve image file location from specified source
      navigator.camera.getPicture(onPhotoURISuccess, onFail, { quality: 50,
        destinationType: destinationType.FILE_URI,
        sourceType: source });
    }

    // Called if something bad happens.
    //
    function onFail(message) {
      alert('Failed because: ' + message);
    }


var fasttrackdb = {};
        fasttrackdb.webdb = {};
        fasttrackdb.webdb.db = null;

        fasttrackdb.webdb.open = function () {
            var dbSize = 5 * 1024 * 1024; // 5MB
            fasttrackdb.webdb.db = openDatabase("FastTrack", "1.0", "FastTrack", dbSize);
        }

        fasttrackdb.webdb.createTable = function () {
           
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                tx.executeSql("CREATE TABLE IF NOT EXISTS users(ID INTEGER PRIMARY KEY ASC, phone TEXT, fullname TEXT,address TEXT, email TEXT, gender TEXT, country TEXT, heartbeat TEXT, status TEXT,position TEXT)", []);
                 
            });
           
        }

        fasttrackdb.webdb.createTablePosts = function () {
           
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                tx.executeSql("CREATE TABLE IF NOT EXISTS posts(ID INTEGER PRIMARY KEY ASC,serverdb_id TEXT, picname TEXT, title TEXT, notes TEXT, type TEXT)", []);
                 
            });
           
        }

        fasttrackdb.webdb.createTableReports = function () {
           
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                tx.executeSql("CREATE TABLE IF NOT EXISTS reports(ID INTEGER PRIMARY KEY ASC,serverdb_id TEXT, picture TEXT, reporter TEXT, message TEXT, type TEXT, position TEXT, time TEXT)", []);
                 
            });
           
        }

        fasttrackdb.webdb.createTableForum = function () {
           
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                tx.executeSql("CREATE TABLE IF NOT EXISTS forum(ID INTEGER PRIMARY KEY ASC, serverdb_id TEXT, forumtopic TEXT, type TEXT)", []);
                 
            });
           
        }

        fasttrackdb.webdb.addForum = function (serverdb_id,forumtopic, type ) {
            //alert("");
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                
                tx.executeSql("INSERT INTO forum(serverdb_id,forumtopic,type) VALUES (?,?,?)",
                    [serverdb_id,forumtopic,type],
                    fasttrackdb.webdb.onSuccess,
                    fasttrackdb.webdb.onError
                    );
            });
        }


        fasttrackdb.webdb.addReport = function (serverdb_id,picture,reporter,message,type,position,time) {
            //alert("");
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                
                tx.executeSql("INSERT INTO reports(serverdb_id,picture,reporter,message,type,position,time) VALUES (?,?,?,?,?,?,?)",
                    [serverdb_id,picture,reporter,message,type,position,time],
                    fasttrackdb.webdb.onSuccess,
                    fasttrackdb.webdb.onError
                    );
            });
        }

        fasttrackdb.webdb.addPost = function (serverdb_id,picname,title,notes,type) {
            //alert("");
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                
                tx.executeSql("INSERT INTO posts(serverdb_id,picname,title,notes,type) VALUES (?,?,?,?,?)",
                    [serverdb_id,picname,title,notes,type],
                    fasttrackdb.webdb.onSuccess,
                    fasttrackdb.webdb.onError
                    );
            });
        }

        fasttrackdb.webdb.onError=function(){
          alert("");
        }

fasttrackdb.webdb.addUser = function (fullname,email,phone,address,country,gender,heartbeat,status,position ) {
            //alert("");
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                
                tx.executeSql("INSERT INTO users(phone,fullname,address,email,gender,country,heartbeat,status,position) VALUES (?,?,?,?,?,?,?,?,?)",
                    [phone,fullname,address,email,gender,country,heartbeat,status,position],
                    fasttrackdb.webdb.onSuccess,
                    fasttrackdb.webdb.onError
                    );
            });
        }
  dbresult=[];
  dblength=0;

function getUsers(query,callback){ // <-- extra param
    
    var db = fasttrackdb.webdb.db;
   db.transaction(function (tx) {
      tx.executeSql(query, [], function(tx, rs){
        dblength=rs.rows.length;
        for(i=0; i<rs.rows.length; i++){
          var row = rs.rows.item(i);
          dbresult[i]={
            fullname:row['fullname'],
            email:row['email'],
            country:row['country'],
            phone:row['phone'],
            gender:row['gender'],
            address:row['address'],
            heartbeat:row['heartbeat'],
            status:row['status'],
            position:row['position']
           
          }
        }
      //  console.log(dbresult);
        callback(dbresult);
        
         //console.log(transactdb);
        // callBack(dblength); // <-- new bit here
      });
   });
   
} 

dbitems=[];

function getPosts(query,callback){ // <-- extra param
 // dbresult="";

    console.log(query);
    var db = fasttrackdb.webdb.db;
   db.transaction(function (tx) {
      tx.executeSql(query, [], function(tx, rs){
        dblength=rs.rows.length;
        for(i=0; i<rs.rows.length; i++){
          var row = rs.rows.item(i);
          dbitems[i]={
            id:row['ID'],
            serverdb_id:row['serverdb_id'],
            title:row['title'],
            notes:row['notes'],
            type:row['type'],
            picname:row['picname']
             
           
          }
        }
      //console.log(dbresult);
     // dbresult=dbitems;
        callback(dbitems); 
      });
   });
   
} 

function getReports(query,callback){ // <-- extra param
     console.log(query);
    var db = fasttrackdb.webdb.db;
   db.transaction(function (tx) {
      tx.executeSql(query, [], function(tx, rs){
        dblength=rs.rows.length;
        
        for(i=0; i<rs.rows.length; i++){
          var row = rs.rows.item(i);
          dbresult[i]={
            message:row['message'],
            reporter:row['reporter'],
            type:row['type'],
            picture:row['picture'],
            serverdb_id:row['serverdb_id'],
            position:row['position'],
            time:row['time']
             
           
          }
        }
      //console.log(dbresult);
        callback(dbresult); 
      });
   });
   
} 



forum=[];

function getForum(query,callback){ // <-- extra param
    console.log(query);
    
    var db = fasttrackdb.webdb.db;
   db.transaction(function (tx) {
      tx.executeSql(query, [], function(tx, rs){
        dblength=rs.rows.length;
        for(i=0; i<rs.rows.length; i++){
          var row = rs.rows.item(i);
          forum[i]={
            id:row['ID'],
            forumtopic:row['forumtopic'],
            serverdb_id:row['serverdb_id'],
            type:row['type']
            
           
          }
        }
      //  console.log(dbresult);
        callback(forum);
        
         //console.log(transactdb);
        // callBack(dblength); // <-- new bit here
      });
   });
   
} 

//var baseurl="http://localhost/mobileapps/cordovapush/hello/dashboard";
var baseurl="http://www.gsffuta.com/dashboard";
//var ROOT = "http://localhost/mobileapps/cordovapush/hello/dashboard/admin";
var ROOT="http://www.gsffuta.com/dashboard/admin";
 API = ROOT;
 console.log(API);
pinbox = "";
var model = new Iugo({
    firstname: "",
    fullname: "",
    email:"",
    aop:"",
    has_attend:"",
    not_attend:"",
    total_cases:"",
     patientname:"",
     case_content:"",
     emergency:  {
         title:"",
         content:"",
         picture:"",

     },
     case:  {
         content:"",
         picture:"",
         type:"",
         reporter:""


     }
      
});

function loadjscssfile(filename, filetype){
 if (filetype=="js"){ //if filename is a external JavaScript file
  var fileref=document.createElement('script')
  fileref.setAttribute("type","text/javascript")
  fileref.setAttribute("src", filename)
 }
 else if (filetype=="css"){ //if filename is an external CSS file
  var fileref=document.createElement("link")
  fileref.setAttribute("rel", "stylesheet")
  fileref.setAttribute("type", "text/css")
  fileref.setAttribute("href", filename)
 }
 if (typeof fileref!="undefined")
  document.getElementsByTagName("head")[0].appendChild(fileref)
}



function loadPhone()
{
  if ($('#register_phone').val().length  == 0)
  {
    $('#register_phone').val('+234');
  }
}

function loadPhone2()
{
  if ($('#login_phone').val().length  == 0)
  {
    $('#login_phone').val('+234');
  }
}

function formatToNaira(num) {
      
      
    var p = num.toFixed(2).split(".");
    return  p[0].split("").reverse().reduce(function(acc, num, i, orig) {
        return   num + (i && !(i % 3) ? "," : "") + acc;
    }, "") + "." + p[1];
}

function switchMenu(page)
{
  console.log(page);
  
  app.menu.setMainPage(page);
   app.menu.closeMenu();


  app.menu.off("postclose", function() {

   
  
});


  app.menu.on("postclose", function() {
 
if (page=="transactions.html") {
  setTimeout(getTransactions,1000);

  //alert('');
};

});

 


 
}

function user_type(user){

   usercat=user;
  if(user=="user"){
    $("#login_pin").hide();
  }else{
     $("#login_pin").show()
  }
}

questionIndex=0;

function nextSignup(){

questionIndex+=1;
usertype=$("#usertype").val();

agenttype=$("#mfc").val();
var fullname=  ( $('input#fullname').val());
var phone =  ( $('input#Phonenumber').val());
var email =  ( $('input#email').val());
var pin =  ( $('input#pin').val());

 
 console.log("usertype" +usertype);    


  
  if(questionIndex==0){
    $("#question").text("Are you an Emergency Agent");
  }

  if((usertype=="Yes")){

    if(questionIndex==1){
     $("#question").text("Which Emergency agency do you belong");
     $("#details").append('<select class="width-full" id="mfc"><option >Medical</option><option>MFC</option><option>Fire</option><option>Crime</option></select>')
     $("#usertype").hide();
  }

  if(questionIndex==2){
     $("#question").text("What is your name?");
     $("#details").append('<input type="text" class="text-input--underbar width-full"  placeholder="fullname" value="" id="fullname" >');
      $("#mfc").hide();
  }

  if(questionIndex==3){
     $("#question").text("Your Phone Number and Email Address?");
     $("#details").append('<input type="text" class="text-input--underbar width-full"  placeholder="Phonenumber" value="" id="Phonenumber" ><input type="text" class="text-input--underbar width-full"  placeholder="Email Address" value="" id="email" >');
      $("#fullname").hide();
  }

  if(questionIndex==4){
     $("#question").text("Your 5 digit fastTrack Token?");
     $("#details").append('<input type="tel" class="text-input--underbar width-full"  placeholder="Pin" value="" id="pin" >');
     $("#email").hide();
     $("#Phonenumber").hide();
  }

  if(questionIndex==5){
    var capture_pin =  ( $('input#pin').val()); 

  if (capture_pin == "") {
      showalert("Please Enter a Valid PIN");
      $('input#pin').focus();
      
      questionIndex=4
      return;
  }
  
  if (capture_pin.length != 5) {
      showalert("PIN must be exactly 5 digits");
      $('input#pin').focus(); 
      questionIndex=4;
      return
  } 
 
  
  var hashpass = CryptoJS.MD5(capture_pin);
  pin = hashpass.toString(); 
  }

  if (questionIndex==5){
    
    registerAgent(fullname,agenttype,email,phone,pin);
  }

  if(questionIndex==6){
      signupmodal.hide();
      main_navigator.pushPage('link.html');
    
    setTimeout(usermap2,300);
    localStorage.setItem("usertype","agent");
  }


  }else{

     

  if(questionIndex==1){
     $("#question").text("What is your name?");
     $("#details").append('<input type="text" class="text-input--underbar width-full"  placeholder="fullname" value="" id="fullname" >');
      $("#usertype").hide();
  }

  if(questionIndex==2){
     $("#question").text("Your Phone Number and Email Address?");
     $("#details").append('<input type="text" class="text-input--underbar width-full"  placeholder="Phonenumber" value="" id="Phonenumber" ><input type="text" class="text-input--underbar width-full"  placeholder="Email Address" value="" id="email" >');
      $("#fullname").hide();
  }

  if(questionIndex==3){
    registerUser(fullname,email,phone);
  }

  if(questionIndex==4){
    signupmodal.hide();
    main_navigator.pushPage('link.html');
    setTimeout(usermap2,300);
  }


  }
}

function showSignup(){
console.log("ddd");
  signupmodal.show();

  return;
   
  console.log(alternate);
  alternate=0;

  //main_navigator.pushPage("signup.html", { animation: "lift" }):
   main_navigator.pushPage('signup.html',{ animation: "lift" }); 
  setTimeout(hideForm,150);

}

 

alternate=0;

 



 
function registerAgent(fullname,agenttype,email,phone,pin)
{
      regid=localStorage.deviceimei,
modal.show();
signupmodal.hide();
  var request = $.ajax({
  url: API + '/registeragent/',
  type: "POST",
   data: {
         email: email,
         phone: phone,
         fullname: fullname,
         agenttype: agenttype,
         pin: pin,
         regid:regid,
         latitude:location.lat,
         longitude:location.lng 
        // secret : 'f9b45f5b3e6f1737804dc5d1f9c39202'
         }
  
});

  
  request.done(function( msg ) {
    //showalert("")
    //msg=JSON.stringify(msg);
    modal.hide();
    signupmodal.show();
    $("#question").html("Now its time to make the world a safer place to live<br/>click next to get started");
    $("#details").hide();
    localStorage.setItem('profile',msg); 
    msg=jQuery.parseJSON(localStorage.profile);
    profile=jQuery.parseJSON(localStorage.profile);
    console.log(msg.fullname);  
    if(msg.fullname != ""){
         
  }
 
  
});

request.error(function( msg ) {
  unspin();
  signupmodal.hide();
  modal.hide();
  questionIndex=0;
  console.log(msg);
  alert('check internet connection');
});
  
}


function registerUser(fullname,email,phone)
{

  console.log(location.lat);

  regid=localStorage.deviceimei;
  
  modal.show();
  signupmodal.hide()
  var request = $.ajax({
  url: API + '/registeruser/',
  type: "POST",
  timeout:20000,
   data: {
         email: email,
         phone: phone,
         fullname: fullname,
         regid:regid,
         latitude:location.lat,
         longitude:location.lng 
        // secret : 'f9b45f5b3e6f1737804dc5d1f9c39202'
         }
  
});

  
  request.done(function( msg ) {
    //showalert("")
    //msg=JSON.stringify(msg);
    signupmodal.show();
     modal.hide();
    $("#question").html("Now its time to make the world a safer place to live<br/>click next to get started");
    $("#details").hide();
    localStorage.setItem("usertype","user");
    localStorage.setItem("profile",msg);
    //main_navigator.pushPage('user.html');
 
  
});

request.error(function( msg ) {
  signupmodal.hide();
  modal.hide();
  questionIndex=0;
  console.log(msg);
  alert('check internet connection');
});
  
}
function pushReg(agenttype){
  if(agenttype=="Crime"){
    initPushwooshCrime();
  }else if(agenttype=="Health"){
    initPushwooshHealth();

  }else{
    initPushwooshFire();
  }
}

function account(){
  profile=jQuery.parseJSON(localStorage.profile);
  console.log(profile);

  model.fullname=profile.fullname
  model.email=profile.email;
  model.aop="Agent Type: "+profile.agenttype;
  model.has_attend=hstatus;
  model.not_attend=nstatus;
  model.total_cases=dblength;
}

nstatus=0;
hstatus=0;

function loginuser(){
  main_navigator.pushPage('user.html');
}

device_id='agent';

function alterlogin(){
  if(device_id=="agent"){
    device_id="user"
  }else{
    device_id="agent";
  }
}

idd=0;


function showactivity(){
  
  $(".timeline").html("");
  dbresult=[];


  getReports("select * from reports group by reporter", function(callback){
    picture=''; iconn='';

    if(dbresult.length==0){
      $(".timeline").append('<ons-list-item class="timeline-li ng-scope list__item ons-list-item-inner list__item--tappable" modifier="tappable"><span class="colortext">No Reported Case Yet</span></ons-list-item>');
      return;
    }

    for(i=0; i<dbresult.length; i++){
      serverdb_id=dbresult[i].serverdb_id;

      picture="images/"+articletype(dbresult[i].type);

      if(dbresult[i].picture!=""){
        picture="data:image/jpg;base64,"+dbresult[i].picture;
      }

     if(i % 2 ==0){
      nstatus+=1;
        iconn='<i class="fa fa-ambulance fa-2x status0"></i>';
      }else{
        hstatus+=1;
        iconn='<i class="fa fa-check fa-2x status1"></i>';
      }

      bgcolor=loadColors();

      picture=initials(dbresult[i].reporter);

      message=trimText(dbresult[i].message);

      $(".timeline").append('<ons-list-item onclick="reporterCases('+i+')" class="timeline-li ng-scope list__item ons-list-item-inner list__item--tappable" modifier="tappable" ><ons-row class="row ons-row-inner"><ons-col width="50px" class="col ons-col-inner" style="-webkit-box-flex: 0; flex: 0 0 50px; max-width: 50px;"><div class="dp" style="background-color:'+bgcolor+';">'+picture+'</div></ons-col><ons-col class="col ons-col-inner"><div class="timeline-date">'+dbresult[i].type+'</div><div class="timline-from"><span class="timeline-name"><b>'+dbresult[i].reporter+'</b></span><span class="timeline-id"></span></div><div class="timeline-message"> <i>'+message+'</i></div></ons-col></ons-row></ons-list-item>');
      
    }
  });
  
}

function reporterCases(index){

  var reporter = dbresult[index].reporter;

  dbresult=[];
 
  $(".timeline").html("");
  if(typeof(localStorage.profile)!="undefined"){
  profile=jQuery.parseJSON(localStorage.profile); 

  model.firstname=profile.fullname;
}
  getReports("select * from reports where reporter = '"+reporter+"'", function(callback){
    picture=''; iconn='';
    console.log(dbresult);

    if(dbresult.length==0){
      $(".timeline").append('<ons-list-item class="timeline-li ng-scope list__item ons-list-item-inner list__item--tappable" modifier="tappable"><span class="colortext">No Reported Case Yet</span></ons-list-item>');
      return;
    }

    for(i=0; i<dbresult.length; i++){
      serverdb_id=dbresult[i].serverdb_id;

      picture="images/"+articletype(dbresult[i].type);

      if(dbresult[i].picture!=""){
        picture="data:image/jpg;base64,"+dbresult[i].picture;
      }

     if(i % 2 ==0){
      nstatus+=1;
        iconn='<i class="fa fa-ambulance fa-2x status0"></i>';
      }else{
        hstatus+=1;
        iconn='<i class="fa fa-check fa-2x status1"></i>';
      }

      bgcolor=loadColors();



      if(dbresult[i].type=="fire"){
        picture="F";
      }else if(dbresult[i].type=="health"){
        picture="M";
      }else if(dbresult[i].type=="crime"){
        picture="C";
      }else{
        picture="O";
        //loadCustomPics(dbresult[i].serverdb_id);
      }

      dbresult[i].type[0].toUpperCase();

      usertimeline='<div class="card">'+
      '<div class="card-header"><div class="media"><div class="pull-left">'+
      '<div class="dp" style="background-color:'+bgcolor+';">'+picture+'</div></div>'+
      '<div class="media-body m-t-5">'+
      '<h2>'+dbresult[i].type+' Alert <small>'+dbresult[i].reporter+' Reported on '+dbresult[i].time+'</small></h2>'+
      '</div></div></div><div class="card-body card-padding p-t-0 contentpod"><p>'+dbresult[i].message+'</p>'+
      '<div id="pics'+dbresult[i].serverdb_id+'"></div><ul class="wall-attrs clearfix list-inline list-unstyled">'+
      '<li class="wa-users"><a href="#"><button class="btn btn-sm btn-primary" onclick="casedetails('+i+')">Post</button>'+
      '</a></li></ul></div></div>';
 
      

     
      $("#mywall").append(usertimeline);
    }
  });
  
}




function casedetails(id){
  idd=id;
  main_navigator.pushPage('case.html');

  var report_msg=$("#reportmsg").val();
   

   if(dbresult[idd].position==""){
    caseposition='<ons-list-item class="timeline-li ng-scope list__item ons-list-item-inner list__item--tappable" modifier="tappable"><span class="colortext">Case Location Not Available</span></ons-list-item>';
   $("#usermap").hide();
   }else{
   latlng=dbresult[idd].position.split(",");

    
   destination.lat=latlng[0];
   destination.lng=latlng[1]; 
    caseposition='';
   }

   if(dbresult[id].picture==""){
    picture ="images/"+articletype(dbresult[idd].type);
  }else{
    picture="data:image/jpg;base64,"+dbresult[id].picture
  } 
  
  if(dbresult[idd].type=="fire"){
        picture="F";
      }else if(dbresult[idd].type=="health"){
        picture="M";
      }else if(dbresult[idd].type=="crime"){
        picture="C";
      }else{
        picture="O";
      } 
  
   setTimeout(function(){
    
    model.case.reporter=dbresult[idd].reporter;
    model.case.content=dbresult[idd].message;

    reportimg=' <div class="wip-item" data-src="img/headers/4.png" style="background-image: url('+baseurl+'/Reportpics/Reportpics'+dbresult[idd].serverdb_id+'.jpg);">'+
                                            '<div class="lightbox-item"></div>'+
                   '</div>';

    if(dbresult[idd].type=="Custom"){
      $(".wall-img-preview").html(reportimg);
    }
    
    usermap3(destination);
    $(".dp").text(picture);
    $("#reporttype").text(dbresult[idd].type);
   },100);

   reportid=(dbresult[idd].serverdb_id);
   reporter=profile.fullname;

  var request = $.ajax({
  url: API + '/getComments/',
  type: "POST",
  timeout:20000,
   data: {
    comment:report_msg,
    reportid:dbresult[idd].serverdb_id,
    reporter:reporter
           
         }
  
});

  
  request.done(function( msg ) {
 
    //successDialog("Nice post from you, keep posting to help other people get updated information of the scene");
    msg=jQuery.parseJSON(msg);
    commentlist="";

    lastreporter='';

    for(j=0; j<msg.length; j++){
      picture=initials(msg[j].reporter);
 

      commentlist +='<div class="media">'+
         '<a href="#" class="pull-left"><div class="dp" style="background-color:'+loadColors()+'">'+picture+'</div></a>'+
         ' <div class="media-body"><a href="#" class="a-title">'+msg[j].reporter+'</a> '+
         '<small class="c-gray m-l-10"></small>'+

         '<p class="comments">'+msg[j].comment+'</p>'+
          '</div></div>';


    }
    console.log(msg);
    $(".commentlist").append(commentlist);
     
});

request.error(function( msg ) {
  unspin();
  alert('check internet connection');
});
 
}


function successDialog(message){
   swal("Good job!", message, "success")
}

function commentReport(){
  comment=$("#commentmsg").val();

  reporter=profile.fullname;
  console.log(reporter);

  if(comment==""){
    swal("oops!, looks like we have an empty comment");
    return;
  }

  reportid=(dbresult[idd].serverdb_id);

  var request = $.ajax({
  url: API + '/getComments/',
  type: "POST",
  timeout:20000,
   data: {
          reportid:reportid,
          comment:comment,
          reporter:reporter
         }
  
});

  
  request.done(function( msg ) {

    successDialog("Nice post from you, keep posting to help other people get updated information of the scene");
    msg=jQuery.parseJSON(msg);
    commentlist="";

    for(j=0; j<msg.length; j++){
      commentlist +='<div class="media">'+
         '<a href="#" class="pull-left"><div class="dp" style="background-color:'+loadColors()+'">'+picture+'</div></a>'+
         ' <div class="media-body"><a href="#" class="a-title">'+msg[j].reporter+'</a> '+
         '<small class="c-gray m-l-10">3 mins ago...</small>'+
         '<p class="comments">'+msg[j].comment+'</p>'+
          '</div></div>';
    }
    console.log(msg);
    $(".commentlist").append(commentlist);
    showactivity();
     
  
});

request.error(function( msg ) {
  unspin();
  alert('check internet connection');
});
}

serverdb_id=0;

function pullReports(){
  modal.show();
  getPosts("select * from reports", function(callback){
    id=0;
    for(i=0;i<dbresult.length; i++){
      id=dbresult[i].serverdb_id;
    }
    if(id==""){
      id=0;
    }

    console.log(id);

     

    profile=jQuery.parseJSON(localStorage.profile);

  $("#loadmore").text("Loading...");
  var request = $.ajax({
  url: API + '/getReports/',
  type: "POST",
  timeout:20000,
   data: {
          id:id
           
         }
  
});

  
  request.done(function( msg ) {
    $("#loadmore").text("Load more");
    unspin();
     
    msg=jQuery.parseJSON(msg);
    console.log(msg);

    if(msg.length > 0){
    
    for(i=0; i<msg.length; i++){
      
    fasttrackdb.webdb.addReport(msg[i].id,'msg[i].picture',msg[i].reporter,msg[i].message,msg[i].type,msg[i].position,msg[i].time);
    }
     
    }else{
      showalert("No new Emergency Alert at the moment");
    }

    showactivity();
     
  
});

request.error(function( msg ) {
  $("#loadmore").text("Load more");
  unspin();
  console.log(msg);
  alert('check internet connection');
});
});

}

function articleinit(){

}

function pullArticles(){
  modal.show();
  getPosts("select * from posts", function(callback){
    id="";

    for(i=0;i<dbitems.length; i++){
      id=dbitems[i].serverdb_id;
    }
    if(id==""){
      id=0;
    }

    console.log(id)

  $("#loadmore").text("Loading...");
  

  var request = $.ajax({
  url: API + '/getPosts/',
  type: "POST",
  timeout:20000,
   data: {
          id:id
         }
  
});

  
  request.done(function( msg ) {
    console.log(msg);
    modal.hide();

    $("#loadmore").text("Load more");
    unspin();
     
    msg=jQuery.parseJSON(msg);
    console.log(msg);

    if(msg.length > 0){
    
    for(i=0; i<msg.length; i++){
      
    fasttrackdb.webdb.addPost(msg[i].id,msg[i].picname,msg[i].post_title,msg[i].post_notes,msg[i].post_category);
    }
     
    }

    //showArticles();
     showalert("News updated");
  
});

request.error(function( msg ) {
  $("#loadmore").text("Load more");
  modal.hide();
  console.log(msg);
  alert('check internet connection');
});
});

}

newstype="";

function showArticles(type){
  newstype=type
  dbitems=[];
  console.log(dbitems);

  $(".timeline").html("");
   
     main_navigator.pushPage('articles.html',{ animation: "slide" }); 

     setTimeout(function(){
    getPosts("select * from posts where type = '"+type+"'", function(callback){
    
    console.log(dbitems);

    if(dbitems.length==0){
      $(".timeline").append('<ons-list-item class="timeline-li ng-scope list__item ons-list-item-inner list__item--tappable" modifier="tappable"><span class="colortext">oops! help is coming soon</span></ons-list-item>');
      return;
    }

    for(i=0; i<dbitems.length; i++){ 

      id=dbitems[i].id;
      
     if(i % 2 ==0){
      nstatus+=1;
        iconn='<i class="fa fa-ambulance fa-2x status0"></i>';
      }else{
        hstatus+=1;
        iconn='<i class="fa fa-check fa-2x status1"></i>';
      }

      picture=articletype(dbitems[i].type); 

      if(dbitems[i].picname==""){
        dbitems[i].picname="images/"+dbitems[i].type+".jpg";
      }

      $(".timeline").append('<ons-list-item onclick="article('+i+')" class="timeline-li ng-scope list__item ons-list-item-inner list__item--tappable" modifier="tappable" ><ons-row class="row ons-row-inner"><ons-col width="50px" class="col ons-col-inner" style="-webkit-box-flex: 0; flex: 0 0 50px; max-width: 50px;"><img  class="timeline-image" src="'+dbitems[i].picname+'"/></ons-col><ons-col class="col ons-col-inner"><div class="timeline-date">'+iconn+'</div><div class="timline-from"><span class="timeline-name"><b>'+dbitems[i].title+'</span><span class="timeline-id"></span></div><div class="timeline-message">Type: <i>'+dbitems[i].type+'</i></div></ons-col></ons-row></ons-list-item>');
    
    }

  });
   },100);
  
   return; 
}

function listArticles(){
   
}

function articletype(type){
  type= type.toLowerCase();
   if(type=="fire"){
       return 'ambulance.png';
      }else if(type=="health"){
        return 'doctor.png'
      }else if(type=="crime"){
        return 'police.ico';
      }else{
       return 'ambulance.png';
      }
}

function article(id){
   idd=id;
  main_navigator.pushPage('emergency.html');
  picture =articletype(dbitems[idd].type);
  setTimeout(function(){

      if(dbitems[id].picname==""){
        dbitems[id].picname="images/"+dbitems[id].type+".jpg";
      }

    $(".case_status").append('<img style="height:200px;" src="'+dbitems[idd].picname+'"/>');
     model.emergency.title=dbitems[idd].title;
     //model.emergency.content=dbitems[idd].notes;
     $("#case_content").html(dbitems[idd].notes);
    
   },100);
  
}

 



 

function listChatPals(){
  $(".timeline").append('<ons-list-item class="timeline-li ng-scope list__item ons-list-item-inner list__item--tappable" modifier="tappable"><span class="colortext">No Messages Yet</span></ons-list-item>');
}




function usermap(){
 // getLocation();

  //alert(location.lat);

  if(typeof(google)=="undefined"){
    return;
  }
           var mapCanvas = document.getElementById('userlocation');
           console.log("location is "+location.lat," , "+location.lng);
            var mapOptions = {
          //6.5243793/3.3792057

          center: new google.maps.LatLng(location.lat,location.lng),
          zoom: 8,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }


        var infoWindow = new google.maps.InfoWindow();
        var map = new google.maps.Map(mapCanvas, mapOptions)

        var myLatlng = new google.maps.LatLng(location.lat,location.lng);
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: "Mumbai"
        });
        }   


        function usermap3(dest){
         // getLocation();

          if(typeof(google)=="undefined"){
            return;
          }

          destination=dest;

          console.log(destination);
          
           var mapCanvas = document.getElementById('usermap');
           console.log("location is "+location.lat," , "+location.lng);

            var mapOptions = { 
          center: new google.maps.LatLng(destination.lat,destination.lng),
          zoom: 16,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;

        var infoWindow = new google.maps.InfoWindow();
        var map = new google.maps.Map(mapCanvas, mapOptions);

         var myLatlng = new google.maps.LatLng(destination.lat,destination.lng);
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: "Nigeria"
        });

        

        directionsDisplay.setMap(map);

        calculateAndDisplayRoute(directionsService, directionsDisplay);

       


        } 

destination=[];
  function calculateAndDisplayRoute(directionsService, directionsDisplay) {
    console.log(location.lat+','+location.lng+" "+destination);
  directionsService.route({
    origin: location.lat+','+location.lng,
    destination: destination.lat+','+destination.lng,
    travelMode: google.maps.TravelMode.DRIVING
  }, function(response, status) {
    if (status === google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
      console.log(response.routes[0].legs[0].steps);
      drivingInstructions=response.routes[0].legs[0].steps;
    } else {
      window.alert('Directions request failed due to ' + status);
    }
  });
} 

drivingInstructions=[];

instructionId=-1;

function giveDirections(instructionId){
  if(instructionId > drivingInstructions.length){
    showalert("Directions Ended");
    return;
  }

  if(instructionId < 0){
    //showalert("Directions Ended");
    return;
  }

$("#instruction").html(drivingInstructions[instructionId].instructions);
} 

function previousDirection(){
instructionId-=1;
  if(instructionId <=0){
    instructionId=0;
  }

giveDirections(instructionId);
}

function nextDirection(){
  instructionId+=1;
  if(instructionId > drivingInstructions.length-1){
    instructionId=drivingInstructions.length-1;
  }
  giveDirections(instructionId);
}

   function usermap2(){
         //   getLocation();

           if(typeof(google)=="undefined"){
            return;
            }
           
           var mapCanvas = document.getElementById('mapBox');
           console.log("location is "+location.lat," , "+location.lng);
            var mapOptions = {
          //6.5243793/3.3792057

        center: new google.maps.LatLng(location.lat,location.lng),
        //center: new google.maps.LatrootLng("18.9750", "72.8258"),
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var infoWindow = new google.maps.InfoWindow();
        var map = new google.maps.Map(mapCanvas, mapOptions)

        var myLatlng = new google.maps.LatLng(location.lat,location.lng);
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: "Mumbai"
        });
        }



function setStats(){
  setTimeout(function(){

    if(typeof(localStorage.profile)!="undefined"){
        profile=jQuery.parseJSON(localStorage.profile);
        console.log(profile.fullname);
        model.firstname=profile.fullname;
      }

      getReports("select * from reports", function(callback){
    fire_reports=0;
    medical_reports=0;
    crime_reports=0;
      console.log(dbresult);
      for(j=0; j<dbresult.length; j++){
        if(dbresult[j].type=="crime"){
          crime_reports+=1;
        }else if(dbresult[j].type=="fire"){
          fire_reports+=1;
        }else if(dbresult[j].type=="health"){
          medical_reports+=1;
        }
      }
      console.log(crime_reports);
      $("#fire_reports").text(fire_reports+" Fire Reports");
      $("#crime_reports").text(crime_reports+" Crime Reports");
      $("#medical_reports").text(medical_reports+" Medical Reports");
   });
 
   },100);
}   


function logininit(){
 //main_navigator.pushPage('user.html');
 //return;

   

 if(typeof(google)!="undefined"){

  google.maps.event.addDomListener(window, 'load', init);

        var map;
      }

 if((typeof(localStorage.usertype) !="undefined") & (localStorage.usertype=="agent")){
    main_navigator.pushPage('pin.html',{ animation: "lift" });
    //setTimeout(usermap2,100);
    setTimeout(setStats,100);
    return;
 }

 if((typeof(localStorage.usertype)!="undefined")&(localStorage.usertype=="user")){
     // setTimeout(usermap2,100); 
      main_navigator.pushPage('link.html',{ animation: "lift" });

      if(typeof(localStorage.profile)!="undefined"){
        profile=jQuery.parseJSON(localStorage.profile);
        console.log(profile.fullname);
        model.firstname=profile.fullname;
      }

      setTimeout(setStats,100);
      return;
 }
 
  loginmodal.show();
 return;


 
   
}

function userlogin(){
  spin(); 
  var pin =  ( $('input#login_pin').val());  
  var phone = $('input#login_phone').val();
  
  if (phone == "") {
      showalert("Please Enter your Phone Number");
      $('input#login_phone').focus(); 
        unspin();
      return
    } 

   

   if((typeof(localStorage.userprofile) !="undefined") && (localStorage.userprofile !="")){
   profile=jQuery.parseJSON(localStorage.userprofile);
   
      if(phone==profile.phone){
        main_navigator.pushPage('link.html');
        setTimeout(usermap2,100); 
        return; 
      }
   
    }else{

     var request = $.ajax({
          url: API + '/verifyuser/',
          type: "POST",
          data: {
              phone: phone
          }
  
});

  
request.done(function( msg ) {
  unspin();
  loginmodal.hide(); 
  msg=jQuery.parseJSON(msg);
  profile=msg;
  model.firstname=profile.fullname; 
  
  if(msg.length > 0){ 
    msg=JSON.stringify(msg[0]);
    main_navigator.pushPage('link.html');
    setStats();
    localStorage.setItem("profile",msg);
    localStorage.setItem("usertype","user");
    setTimeout(usermap2,100); 
}else{
  showalert("Invalid Pin or Phonenumber");

}
   
      
  
});

request.error(function( msg ) {
  unspin();
  console.log(msg);
   setTimeout(showactivity,300); 
  //alert('check internet connection');
});
            

  }

}

profile=[];

usercat=0;

function login()
{

   

  if(usercat=="0"){
    showalert("Please Select MFC or User login");
    return;
  }
  
  if(usercat=="user"){
    userlogin();
    return;
  }
   

spin(); 
  
var pin =  ( $('input#login_pin').val()); 
    if (pin == "") {
      showalert("Please Enter your PIN Number.");
      $('input#login_pin').focus(); 
      unspin();
      return
    }
  
  var phone = $('input#login_phone').val();
   if (phone == "") {
      showalert("Please Enter your Phone Number");
      $('input#login_phone').focus(); 
        unspin();
      return
    }


  var hashpass = CryptoJS.MD5(pin);
  hashpass = hashpass.toString();

   if(typeof(localStorage.profile) !="undefined"){
      profile=jQuery.parseJSON(localStorage.profile);
   
      if(phone==profile.phone){
          main_navigator.pushPage('link.html');
          setTimeout(usermap2,300);
          return; 
   }
   
  } 

   var request = $.ajax({
        url: API + '/verifyagent/',
        type: "POST",
        data: {
         phone: phone, 
         password: hashpass, 
         }
  
    });

  
    request.done(function( msg ) {
        unspin();
        loginmodal.hide(); 
        msg=jQuery.parseJSON(msg); 
        profile=msg; 
        model.firstname=profile.fullname;
   
        if(msg.length > 0){ 
            msg=JSON.stringify(msg[0]); 
            loginmodal.hide();
            setStats();
            main_navigator.pushPage('link.html');
            localStorage.setItem("profile",msg);
            localStorage.setItem("usertype","agent");
            setTimeout(usermap2,300); 
        }else{
            showalert("Invalid Pin or Phonenumber"); 
         } 
    });

request.error(function( msg ) {
  unspin();
  loginmodal.hide();
  alert('check internet connection');
});
            
  
}

function reload(){
  window.location.reload();
  return;
   main_navigator.pushPage('start.html',{ animation: "none" });
}


 
























function initPushwoosh()
{
   // alert('push started');
    var pushNotification = window.plugins.pushNotification;
 
    //set push notifications handler
    document.addEventListener('push-notification', function(event) {
        var title = event.notification.title;
        var userData = event.notification.userdata;
                                 
        if(typeof(userData) != "undefined") {
            console.warn('user data: ' + JSON.stringify(userData));
        }
                                     
        alert(title);
    });
 
    //initialize Pushwoosh with projectid: "GOOGLE_PROJECT_NUMBER", appid : "PUSHWOOSH_APP_ID". This will trigger all pending push notifications on start.
    pushNotification.onDeviceReady({ projectid: "703630268544", appid : "BB556-45C67" });
  //alert('push started2');
    //register for pushes
    pushNotification.registerDevice(
        function(status) {
            var pushToken = status;
    //        alert('push token: ' + pushToken);
        },
        function(status) {
            //console.warn(JSON.stringify(['failed to register ', status]));
       //     alert(JSON.stringify(['failed to register ', status]));
        }
    );
     //alert('push started 3');
}


 //ons.bootstrap();

 function getLocation(){
  if(navigator.geolocation){
              
               navigator.geolocation.getCurrentPosition(onPositionUpdate);
            }else{
               alert("navigator.geolocation is not available");
             }

       

} 


var onSuccess = function(position) {
    /*alert('Latitude: '          + position.coords.latitude          + '\n' +
          'Longitude: '         + position.coords.longitude         + '\n' +
          'Altitude: '          + position.coords.altitude          + '\n' +
          'Accuracy: '          + position.coords.accuracy          + '\n' +
          'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
          'Heading: '           + position.coords.heading           + '\n' +
          'Speed: '             + position.coords.speed             + '\n' +
          'Timestamp: '         + position.timestamp                + '\n'); */
location.lat=position.coords.latitude;
location.lng=position.coords.longitude;
};

// onError Callback receives a PositionError object
//
function onError(error) {
    alert('code: '    + error.code    + '\n' +
          'message: ' + error.message + '\n');
}

navigator.geolocation.getCurrentPosition(onSuccess, onError);





location.lat='';
location.lng='';




function onPositionUpdate(position)
            {

                 location.lat = position.coords.latitude;
                 location.lng = position.coords.longitude;

                 //alert("Current position: " + location.lat + " " + location.lng);
            }
 

function init()
{
  
navigator.geolocation.getCurrentPosition(onSuccess, onError);
getLocation();


  fasttrackdb.webdb.open();
  fasttrackdb.webdb.createTable();
  fasttrackdb.webdb.createTablePosts();
  fasttrackdb.webdb.createTableReports();
  fasttrackdb.webdb.createTableForum();

main_navigator.pushPage('index.html',{ animation: "none" });

 setTimeout(function(){

     var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
         
         spaceBetween: 30,
        centeredSlides: true,
        autoplay: 2500,
        autoplayDisableOnInteraction: false

         
    });
   },100);

 
    
}

function showothers(){
  main_navigator.pushPage('others.html');
  //$("#push_message").text("My current location is "+location.lat+ " , "+location.lng);
  setTimeout(initothers,300);
}

function initothers () {
 $("#push_message").text("My current location is "+location.lat+ ","+location.lng);
}

function initPushMessage(){
 

}

function pushMessage(pushType) {


  swal({   
                    title: "Are you sure?",   
                    text: "Sending false alarm will disable you from sending and receiving Emergency Alerts!",   
                    type: "warning",   
                    showCancelButton: true,   
                    confirmButtonColor: "#DD6B55",   
                    confirmButtonText: "Yes, Send it!",   
                    cancelButtonText: "No, Cancel!",   
                    closeOnConfirm: false,   
                    closeOnCancel: false 
                }, function(isConfirm){   
                    if (isConfirm) {  
                    main_navigator.pushPage('gps.html');
   setTimeout(initPushMessage,100);
   setTimeout(usermap,100);
    
   linkname="";
   if(pushType=="crime"){
    linkname='/pushCrime';
    message="There is an ongoing criminal activity going on. Please respond urgently and safe lives and properties";
   }else if(pushType=='fire'){
    linkname='/pushFire';
    message="There is a fire outbreak. Please respond urgently and safe lives and properties";
   }else{
    linkname='/pushHealth';
    message="A medical attention is needed. Please respond urgently and safe lives and properties";
   }

  reporter=profile.fullname;
  //picture=imgreport;

  console.log(reporter);
    
   type=pushType;

   var time = new Date();
   time=time.toString();

   split_date=time.split(" ");
   time=split_date[0]+" "+split_date[1]+" "+split_date[2]+" "+split_date[3]+" at "+split_date[4];
    

   var request = $.ajax({
  url: API + linkname,
  type: "POST",
   data: {
         message: message,
         reporter:reporter,
        
         type:type,
         position:location.lat+","+location.lng,
         time:time
          
        // secret : 'f9b45f5b3e6f1737804dc5d1f9c39202'
         }
  
});

  
request.done(function( msg ) {
 console.log(msg);
 $("#pushStatus").html("Emergency agencies has ben notified <br/> and will be with you shortly");
 unspin();
 
      
  
});

request.error(function( msg ) {
  unspin();
  console.log(msg);
  alert('check internet connection');
});   
                        swal("Alerted!", "Your Emergency Alert sent.", "success");   
                    } else {     
                        swal("Cancelled", "Your Emergency Alert cancelled :)", "error");  
                        return; 
                    } 
                });

  

  //address=http://maps.googleapis.com/maps/api/geocode/json?latlng=6.5980075,3.3546774999999998&sensor=true
   

  
}

function showMessage(){
  main_navigator.pushPage('agents.html');
}

function snapSend(){
  //snapshot();
  notifyUsers();
}


function notifyUsers(){

   
   $("#load_msg").text("Sending Notification to Citizens");
   spin();
  var push_message=$('#push_message').val();
  var post_title=$('#push_title').val();

  if(push_message==""){
    showalert("your message is empty, Please try again");
    $('#push_message').focus();
    unspin();
    return;
  }

  if(post_title==""){
    showalert("your message is empty, Please try again");
    $('#post_title').focus();
    unspin();
    return;
  }
  //push_message=replaceAll(push_message); 
   
  console.log(push_message);

  reporter=profile.reporter;
    
    if(typeof(newstype)=="undefined"){
      type="Custom";
    }else{
      type=newstype;
    }
   
   picture=imgreport;

   if(picture!=""){
   picture="data:image/jpg;base64,"+picture;
    }

news.hide();
    
   console.log(newstype);
 
   
  var request = $.ajax({
  url: API + '/notifyUsers',
  type: "POST",
  timeout:20000,
   data: {
        post_notes:push_message,
        post_title:post_title,
        post_category:type,
        picname:picture

        
        // secret : 'f9b45f5b3e6f1737804dc5d1f9c39202'
         }
  
});

  
request.done(function( msg ) {
unspin();
news.hide();
console.log(msg);
$("#load_msg").text("Citizens Notified");
});

request.error(function( msg ) {
  unspin();
  news.hide();
  console.log(msg);
  alert('check internet connection');
});

}


function updateLocation(){
  console.log(profile);
  console.log(location.lat);

  var request = $.ajax({
  url: API + '/updateLocation',
  type: "POST",
  timeout:20000,
   data: {
          id:profile.id,
         location:location.lat+'/'+location.lng
        // secret : 'f9b45f5b3e6f1737804dc5d1f9c39202'
         }
  
});

  
request.done(function( msg ) {
unspin();
console.log(msg);
showalert("Location updated");
});

request.error(function( msg ) {
  unspin();
  console.log(msg);
  alert('check internet connection');
});

}


function pushSms(){

  swal({   
                    title: "Are you sure?",   
                    text: "Sending false alarm will disable you from sending and receiving Emergency Alerts!",   
                    type: "warning",   
                    showCancelButton: true,   
                    confirmButtonColor: "#DD6B55",   
                    confirmButtonText: "Yes, Send it!",   
                    cancelButtonText: "No, Cancel!",   
                    closeOnConfirm: false,   
                    closeOnCancel: false 
                }, function(isConfirm){   
                    if (isConfirm) { 
                      $("#load_msg").text("Notifying MFC agents via sms ...");
   modal.show();
   var push_message=$('#push_message').val();
  //push_message=replaceAll(push_message); 
   
  console.log(push_message);

  
   reporter=profile.fullname;

   
   picture=imgreport;
    
   type='Custom';
   
  var request = $.ajax({
  url: API + '/sendsms',
  type: "POST",
  timeout:20000,
   data: {
         message:push_message,
         reporter:reporter,
         picture:picture,
         type:type,
         position:location.lat+','+location.lng
        // secret : 'f9b45f5b3e6f1737804dc5d1f9c39202'
         }
  
});

  
request.done(function( msg ) {
unspin();
modal.hide();

console.log(msg);
$("#load_msg").text("MFC agents notified");
});

request.error(function( msg ) {
  unspin();
  modal.hide()
  console.log(msg);
  alert('check internet connection');
});
    
                        swal("Alerted!", "Your Emergency Alert sent.", "success");
                           
                    } else {     
                        swal("Cancelled", "Your Emergency Alert cancelled :)", "error");  
                        return; 
                    } 
                });

   
   
}

 
function initPushwooshHealth()
{
   // alert('push started');
    var pushNotification = window.plugins.pushNotification;
 
    //set push notifications handler
    document.addEventListener('push-notification', function(event) {
        var title = event.notification.title;
        var userData = event.notification.userdata;
                                 
        if(typeof(userData) != "undefined") {
            console.warn('user data: ' + JSON.stringify(userData));
        }
                                     
        alert(title);
    });
 
    //initialize Pushwoosh with projectid: "GOOGLE_PROJECT_NUMBER", appid : "PUSHWOOSH_APP_ID". This will trigger all pending push notifications on start.
    pushNotification.onDeviceReady({ projectid: "703630268544", appid : "75E3D-FAB34" });
 // alert('push started2');
    //register for pushes
    pushNotification.registerDevice(
        function(status) {
            var pushToken = status;
      //      alert('push token: ' + pushToken);
        },
        function(status) {
            //console.warn(JSON.stringify(['failed to register ', status]));
            alert(JSON.stringify(['failed to register ', status]));
        }
    );
    // alert('push started 3');
}

function initPushwooshFire()
{
   // alert('push started');
    var pushNotification = window.plugins.pushNotification;
 
    //set push notifications handler
    document.addEventListener('push-notification', function(event) {
        var title = event.notification.title;
        var userData = event.notification.userdata;
                                 
        if(typeof(userData) != "undefined") {
            console.warn('user data: ' + JSON.stringify(userData));
        }
                                     
        alert(title);
    });
 
    //initialize Pushwoosh with projectid: "GOOGLE_PROJECT_NUMBER", appid : "PUSHWOOSH_APP_ID". This will trigger all pending push notifications on start.
    pushNotification.onDeviceReady({ projectid: "703630268544", appid : "CB083-05399" });
  //alert('push started2');
    //register for pushes
    pushNotification.registerDevice(
        function(status) {
            var pushToken = status;
       //     alert('push token: ' + pushToken);
        },
        function(status) {
            //console.warn(JSON.stringify(['failed to register ', status]));
            alert(JSON.stringify(['failed to register ', status]));
        }
    );
    // alert('push started 3');
}

function initPushwooshCrime()
{
   // alert('push started');
    var pushNotification = window.plugins.pushNotification;
 
    //set push notifications handler
    document.addEventListener('push-notification', function(event) {
        var title = event.notification.title;
        var userData = event.notification.userdata;
                                 
        if(typeof(userData) != "undefined") {
            console.warn('user data: ' + JSON.stringify(userData));
        }
                                     
        alert(title);
    });
 
    //initialize Pushwoosh with projectid: "GOOGLE_PROJECT_NUMBER", appid : "PUSHWOOSH_APP_ID". This will trigger all pending push notifications on start.
    pushNotification.onDeviceReady({ projectid: "703630268544", appid : "A1E9F-32A6D" });
  //alert('push started2');
    //register for pushes
    pushNotification.registerDevice(
        function(status) {
            var pushToken = status;
            alert('crime push token: ' + pushToken);
        },
        function(status) {
            //console.warn(JSON.stringify(['failed to register ', status]));
            alert(JSON.stringify(['failed to register ', status]));
        }
    );
     //alert('push started 3');
}


 var onCardIOComplete = function(response) {
        
        $('#card_number').val(response.card_number)      
        $('#card_mm').val(response.expiry_month)      
        $('#card_yy').val(response.expiry_year)      
        $('#card_cvv').val(response.cvv)   

        $('#card_number').trigger( "change" );   
  
      };
  
      var onCardIOCancel = function(response) {
      //  showalert ("test")
      //  alert(JSON.stringify(response));
          showalert("card.io scan cancelled");
      };


function cardReader() {
              CardIO.scan({
                  "collect_expiry": true,
                  "collect_cvv": false,
                  "collect_zip": false,
                  "shows_first_use_alert": true,
                  "disable_manual_entry_buttons": false
                },
                onCardIOComplete,
                onCardIOCancel
              );
            }
      



 

 

function fillPin()
{
  $("#pinbox").html("");


  for (var i=0;i<pinbox.length;i++)
  {
  
    $("#pinbox").append("<i class='fa fa-circle'> </i>");
    
  }
   //return;
//console.log(pinbox.length);
  var balance = 5 - pinbox.length;
//console.log(balance);
  for (var i=0;i<balance;i++)
  {
  
    $("#pinbox").append("<i class='fa fa-circle-thin'> </i>");
    
  }

}
function PINEntry(key) {
  
  
  
  if (key == "-") 
    {
      pinbox = pinbox.substr(0, pinbox.length-1);
       $("#pinbox").html("");
      fillPin();
      return;
    }

  else if (pinbox.length < 6) pinbox = pinbox+key;
  
  
  $("#pinbox").html("");
  
  fillPin();
  if (pinbox.length == 5) {
    //console.log(pinbox);
    var hashpass = CryptoJS.MD5(pinbox); 

   profile=jQuery.parseJSON(localStorage.profile);

   var capturepin= (profile.pin); 

    if (hashpass.toString() == capturepin) { 

        setTimeout(usermap2,200);
        main_navigator.pushPage('link.html'); 
        profile=jQuery.parseJSON(localStorage.profile);
      
      
    }
    else {
      authenticated = false;
      
      showalert ('Wrong PIN.');
      
      
    }
    
    pinbox = "";
    $("#pinbox").html("");
    fillPin();
  }
}










function showMenu(){
  main_navigator.pushPage('menu.html');
}

profile=jQuery.parseJSON(localStorage.profile);

function showforum(){
  main_navigator.pushPage('forum.html');

   

  if(typeof(localStorage.profile)!="undefined"){
  profile=jQuery.parseJSON(localStorage.profile);
  console.log(profile);

  model.firstname=profile.fullname;
}

setTimeout(function(){
      getForum("select * from forum", function(callback){
    picture=''; iconn='';

    $('.forum').html("");
    console.log(forum);

    if(forum.length==0){
     // $("#forumstatus").hide();
     $(".forum").append('<ons-list-item class="timeline-li ng-scope list__item ons-list-item-inner list__item--tappable" modifier="tappable"><span class="colortext">No Reported Case Yet</span></ons-list-item>');
      return;
    }
    for(i=0; i<forum.length; i++){
 
     if(i % 2 ==0){
      nstatus+=1;
        iconn='<i class="fa fa- fa-2x status0"></i>';
      }else{
        hstatus+=1;
        iconn='<i class="fa fa-check fa-2x status1"></i>';
      }

      picture=articletype(forum[i].type);
      forumtopic="";

       for(j=0; j<forum[i].forumtopic.length; j++){
        if(j<=30){
        forumtopic+=forum[i].forumtopic[j];
      }
      }
      forumtopic+="...";


      $(".forum").append('<ons-list-item onclick="forumdetails('+forum[i].id+','+i+')" class="timeline-li ng-scope list__item ons-list-item-inner list__item--tappable" modifier="tappable" ><ons-row class="row ons-row-inner"><ons-col width="50px" class="col ons-col-inner" style="-webkit-box-flex: 0; flex: 0 0 50px; max-width: 50px;"><img  class="timeline-image" src="images/'+picture+'"></ons-col><ons-col class="col ons-col-inner"><div class="timeline-date">'+iconn+'</div><div class="timline-from"><span class="timeline-name"><b>'+forumtopic+'</span><span class="timeline-id">'+'  </span></div><div class="timeline-message"></i></div></ons-col></ons-row></ons-list-item>');
    }
  });
   },100);


}


function pullForum(){
  getForum("select * from forum", function(callback){
   
    id=0;

    for(i=0;i<forum.length; i++){
      id=forum[i].serverdb_id;
    }
    if(id==""){
      id=0;
    }

    console.log(id);

  $("#loadforum").text("Loading...");
  // console.log(forum);
  var request = $.ajax({
  url: API + '/getForum/',
  type: "POST",
  timeout:20000,
   data: {
          id:id
         }
  
});

  
  request.done(function( msg ) {
    $("#loadforum").text("Load more");
    unspin();
     
    msg=jQuery.parseJSON(msg);
    console.log(msg);

    if(msg.length > 0){
    
    for(i=0; i<msg.length; i++){
      
    fasttrackdb.webdb.addForum(msg[i].id,msg[i].forumtopic,msg[i].type);
    }
     
    }else{
      showalert("Forum updated");
    }

    showforum();
     
  
});

request.error(function( msg ) {
  $("#loadforum").text("Load more");
  unspin();
  console.log(msg);
  alert('check internet connection');
});
});

}



function forumdetails(id,index){
  idd=id;

  console.log("forum id "+idd);
   main_navigator.pushPage('message.html');
   //setTimeout(pullmessage,100);
   setTimeout(function(){
    console.log(forum);
    $("#forumtopic").text(forum[index].forumtopic);
     $("messages").html("");
  var request = $.ajax({
  url: API + '/pullMessage/',
  type: "POST",
  timeout:20000,
   data: {
          forumid:idd
         }
  
});

  
  request.done(function( msg ) {
 
    unspin();
     
    msg=jQuery.parseJSON(msg);
    console.log(msg);

    if(msg.length==0){
     // $("#forumstatus").hide();
     $(".messages").append('<ons-list-item class="timeline-li ng-scope list__item ons-list-item-inner list__item--tappable" modifier="tappable"><span class="colortext">No Reported Case Yet</span></ons-list-item>');
      return;
    }

    if(msg.length > 0){
    
    for(i=0; i<msg.length; i++){
      
      $("#messages").append("<div class='contentpod msg'><span class='highlight'><i>"+msg[i].sender+':</i></span>'+msg[i].msg+"</div>");
      //$("#msgbody").append("<tr><td><div class='contentpod msg'><span class='highlight'><i>"+msg[i].sender+':</i></span>'+msg[i].msg+"</div></td></tr>");
    }

    $('#bdy')[0].scrollTop = $('#bdy')[0].scrollHeight;
     
    } else{
      $(".messages").append('<ons-list-item class="timeline-li ng-scope list__item ons-list-item-inner list__item--tappable" modifier="tappable"><span class="colortext">No Reported Case Yet</span></ons-list-item>');
      return;
    }

    showactivity();
     
  
});

request.error(function( msg ) {
  $("#loadmore").text("Load more");
  unspin();
  console.log(msg);
  alert('check internet connection');
});
   },100);
}

function pullmessage(){
  

}

function sendMessage(){

  if(typeof(localStorage.profile)!="undefined"){
     profile=jQuery.parseJSON(localStorage.profile);
  }else if(typeof(localStorage.userprofile)!="undefined"){
     profile=jQuery.parseJSON(localStorage.userprofile);
  }else{
    return;
  }

  console.log(profile);

  sender=profile.fullname;
  var msg=( $('#msg').val());
  console.log(msg);

  modal.show();

  var request = $.ajax({
  url: API + '/forumMessage/',
  type: "POST",
  timeout:20000,
   data: {
          forumid:idd,
          msg:msg,
          sender:sender
         }
  
});

  
  request.done(function( msg ) {
    console.log(msg);
    modal.hide();
    $("#messages").html("");
    $('input#msg').val("");
    $('input#msg').focus();

 
    unspin();
     
    msg=jQuery.parseJSON(msg);
    console.log(msg);

    if(msg.length > 0){
    
    for(i=0; i<msg.length; i++){
      
     $("#messages").append("<div class='contentpod msg'><span class='highlight'><i>"+msg[i].sender+':</i></span>'+msg[i].msg+"</div>");
     //$("#msgbody").append("<tr><td><div class='contentpod msg'><span class='highlight'><i>"+msg[i].sender+':</i></span>'+msg[i].msg+"</div></td></tr>");

     
    }

     
$('#bdy')[0].scrollTop = $('#bdy')[0].scrollHeight;
console.log($('#msgbody')[0].scrollHeight);

     
    }else{
      showalert("No Messages");
    }

    //$('#messages').scrollTo(500);
     
  
});

request.error(function( msg ) {
  modal.hide();
  $("#loadmore").text("Load more");
  unspin();
  console.log(msg);
  alert('check internet connection');
});

}































function showalert(message)
{
    try
    {
      // toast.showLong(message);
      window.plugins.toast.showShortCenter(message)
    }
    catch(err)
    {
      alert (message);
    }
}


function validateEmail($email) {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  if( !emailReg.test( $email ) ) {
    return false;
  } else {
    return true;
  }
}

 

function initials(reporter){
  return reporter[0];

   parts=reporter.split(" ");

  
  if (parts.length == 0){
    return parts[0][0];
  }else{
    return parts[0][0]+parts[1][0];
  }
}

function spin()
{
  modal.show();
  $('.nospin').hide();
   $('.spinner').show();
   $('.button--large').attr('disabled','disabled');
}

function unspin()
{
  modal.hide();
  news.hide();
  $('.nospin').show();
   $('.spinner').hide();
 
   $('.button--large').removeAttr('disabled');
} 
 

function deleteMe()
{

  navigator.notification.confirm(
    'Are you sure you want to delete your account?', // message
     onConfirm,            // callback to invoke with index of button pressed
    'Delete? ',           // title
    ['Yes! Delete','No, Cancel']     // buttonLabels
);


}
 


function numberWithCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

 
function copyLink(){
  var text = "copied link";

cordova.plugins.clipboard.copy(text);

cordova.plugins.clipboard.paste(function (text) { alert(text); });
}

function sendmail(){
  window.location='mailto:joshua@kleindevort.com';
}

function snapshot(){
  navigator.camera.getPicture(onCameraSuccess, onFail, { quality: 50,
    destinationType: Camera.DestinationType.DATA_URL
});
}

imgreport='';

function onCameraSuccess(imgdata){
   var image = document.getElementById('myImage');
    image.src = "data:image/jpeg;base64," + imgdata;
    imgreport=imgdata;
    //alert(imgreport);
}

function onCameraFail(message){
  alert('Failed because: ' + message);
}

function sendSms(){
   alert('click');
    var number = document.getElementById('phone_no').value;
        var message = document.getElementById('sms_message').value;
        alert(number);
        alert(message);
        var intent = 'INTENT'; //leave empty for sending sms using default intent
        var success = function () { alert('Message sent successfully'); };
        var error = function (e) { alert('Message Failed:' + e); };
        sms.send(number, message, intent, success, error);

}

function replaceAll(str){
  var new_str='';
  for (var i = str.length - 1; i >= 0; i--) {
    if(str[i]==' '){
      str[i]='-';
      new_str += str[i];
    }

    str=str.replace(' ','-');

  };
  return str;
}

function loadColors(){
  colors=[];
  colors=["#F44336","#E91E63","#9C27B0","#673AB7","#3F51B5","#2196F3","#03A9F4","#00BCD4","#009688","#4CAF50","#8BC34A","#CDDC39","#FFEB3B","#FFC107","#FF9800","#FF5722","#607D8B","#795548"];
  index=Math.random() * (colors.length - 0) + 0;
  
  index=parseInt(index);
   
  return colors[index];
}

function trimText(text){
  newText="";
   for(j=0; j<text.length; j++){
        if(j<=31){
        newText+=text[j];
      }
}
newText+="...";
return newText;
}



$('#sa-params').click(function(){
                swal({   
                    title: "Are you sure?",   
                    text: "You will not be able to recover this imaginary file!",   
                    type: "warning",   
                    showCancelButton: true,   
                    confirmButtonColor: "#DD6B55",   
                    confirmButtonText: "Yes, delete it!",   
                    cancelButtonText: "No, cancel plx!",   
                    closeOnConfirm: false,   
                    closeOnCancel: false 
                }, function(isConfirm){   
                    if (isConfirm) {     
                        swal("Deleted!", "Your imaginary file has been deleted.", "success");   
                    } else {     
                        swal("Cancelled", "Your imaginary file is safe :)", "error");   
                    } 
                });
            });


















